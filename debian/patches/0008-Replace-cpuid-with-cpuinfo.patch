From: Alexander GQ Gerasiov <gq@cs.msu.su>
Date: Fri, 4 Jan 2019 03:04:07 +0300
Subject: Replace cpuid with cpuinfo.

cpuinfo is already shipped in Debian and provides more suitable API.
This commit is not enough for inclusion into upstream codebase. One also
needs to replace cpuid with cpuinfo in the "contrib" directory.

Signed-off-by: Alexander GQ Gerasiov <gq@cs.msu.su>
---
 CMakeLists.txt                                  |  2 +-
 cmake/find_cpuid.cmake                          | 29 -------------
 cmake/find_cpuinfo.cmake                        | 16 ++++++++
 contrib/CMakeLists.txt                          |  4 --
 dbms/CMakeLists.txt                             | 13 +++---
 dbms/src/Common/config.h.in                     |  1 +
 dbms/src/Common/getNumberOfPhysicalCPUCores.cpp | 54 ++++++++-----------------
 7 files changed, 43 insertions(+), 76 deletions(-)
 delete mode 100644 cmake/find_cpuid.cmake
 create mode 100644 cmake/find_cpuinfo.cmake

diff --git a/CMakeLists.txt b/CMakeLists.txt
index 17a56da..549b6e9 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -289,7 +289,7 @@ include (cmake/find_re2.cmake)
 include (cmake/find_rdkafka.cmake)
 include (cmake/find_capnp.cmake)
 include (cmake/find_llvm.cmake)
-include (cmake/find_cpuid.cmake)
+include (cmake/find_cpuinfo.cmake)
 include (cmake/find_libgsasl.cmake)
 include (cmake/find_libxml2.cmake)
 include (cmake/find_hdfs3.cmake)
diff --git a/cmake/find_cpuid.cmake b/cmake/find_cpuid.cmake
deleted file mode 100644
index cda8843..0000000
--- a/cmake/find_cpuid.cmake
+++ /dev/null
@@ -1,29 +0,0 @@
-if (NOT ARCH_ARM)
-    option (USE_INTERNAL_CPUID_LIBRARY "Set to FALSE to use system cpuid library instead of bundled" ${NOT_UNBUNDLED})
-endif ()
-
-#if (USE_INTERNAL_CPUID_LIBRARY AND NOT EXISTS "${ClickHouse_SOURCE_DIR}/contrib/libcpuid/include/cpuid/libcpuid.h")
-#   message (WARNING "submodule contrib/libcpuid is missing. to fix try run: \n git submodule update --init --recursive")
-#   set (USE_INTERNAL_CPUID_LIBRARY 0)
-#   set (MISSING_INTERNAL_CPUID_LIBRARY 1)
-#endif ()
-
-if (NOT USE_INTERNAL_CPUID_LIBRARY)
-    find_library (CPUID_LIBRARY cpuid)
-    find_path (CPUID_INCLUDE_DIR NAMES libcpuid/libcpuid.h PATHS ${CPUID_INCLUDE_PATHS})
-endif ()
-
-if (CPUID_LIBRARY AND CPUID_INCLUDE_DIR)
-    if (OS_FREEBSD)
-        # need in /usr/local/include/libcpuid/libcpuid_types.h
-        # Freebsd: /usr/local/include/libcpuid/libcpuid_types.h:61:29: error: conflicting declaration 'typedef long long int int64_t'
-        add_definitions(-DHAVE_STDINT_H)
-        # TODO: make virtual target cpuid:cpuid with COMPILE_DEFINITIONS property
-    endif ()
-elseif (NOT MISSING_INTERNAL_CPUID_LIBRARY)
-    set (CPUID_INCLUDE_DIR ${ClickHouse_SOURCE_DIR}/contrib/libcpuid/include)
-    set (USE_INTERNAL_CPUID_LIBRARY 1)
-    set (CPUID_LIBRARY cpuid)
-endif ()
-
-message (STATUS "Using cpuid: ${CPUID_INCLUDE_DIR} : ${CPUID_LIBRARY}")
diff --git a/cmake/find_cpuinfo.cmake b/cmake/find_cpuinfo.cmake
new file mode 100644
index 0000000..959119f
--- /dev/null
+++ b/cmake/find_cpuinfo.cmake
@@ -0,0 +1,16 @@
+if (NOT ARCH_ARM)
+    option (USE_INTERNAL_CPUINFO_LIBRARY "Set to FALSE to use system cpuinfo library instead of bundled" ${NOT_UNBUNDLED})
+endif ()
+
+if (NOT USE_INTERNAL_CPUINFO_LIBRARY)
+    find_library (CPUINFO_LIBRARY cpuinfo)
+    find_path (CPUINFO_INCLUDE_DIR NAMES cpuinfo.h PATHS ${CPUINFO_INCLUDE_PATHS})
+endif ()
+
+if (CPUINFO_LIBRARY)
+    set(USE_CPUINFO 1)
+else()
+    set(USE_CPUINFO 0)
+endif ()
+
+message (STATUS "Using cpuinfo: ${CPUINFO_INCLUDE_DIR} : ${CPUINFO_LIBRARY}")
diff --git a/contrib/CMakeLists.txt b/contrib/CMakeLists.txt
index ab453b9..8941e96 100644
--- a/contrib/CMakeLists.txt
+++ b/contrib/CMakeLists.txt
@@ -97,10 +97,6 @@ if (ENABLE_JEMALLOC AND USE_INTERNAL_JEMALLOC_LIBRARY)
     add_subdirectory (jemalloc-cmake)
 endif ()
 
-if (USE_INTERNAL_CPUID_LIBRARY)
-    add_subdirectory (libcpuid)
-endif ()
-
 if (USE_INTERNAL_SSL_LIBRARY)
     if (NOT MAKE_STATIC_LIBRARIES)
         set (BUILD_SHARED 1)
diff --git a/dbms/CMakeLists.txt b/dbms/CMakeLists.txt
index b7f1173..0e4a36a 100644
--- a/dbms/CMakeLists.txt
+++ b/dbms/CMakeLists.txt
@@ -142,17 +142,12 @@ if (CMAKE_BUILD_TYPE_UC STREQUAL "RELEASE" OR CMAKE_BUILD_TYPE_UC STREQUAL "RELW
         PROPERTIES COMPILE_FLAGS -g0)
 endif ()
 
-if (NOT ARCH_ARM AND CPUID_LIBRARY)
-    set (LINK_LIBRARIES_ONLY_ON_X86_64 ${CPUID_LIBRARY})
-endif()
-
 target_link_libraries (clickhouse_common_io
         PUBLIC
     common
         PRIVATE
     string_utils
     widechar_width
-    ${LINK_LIBRARIES_ONLY_ON_X86_64}
     ${LZ4_LIBRARY}
     ${ZSTD_LIBRARY}
     ${DOUBLE_CONVERSION_LIBRARIES}
@@ -175,6 +170,14 @@ target_link_libraries (clickhouse_common_io
     ${CMAKE_DL_LIBS}
 )
 
+if (USE_CPUINFO)
+target_link_libraries (clickhouse_common_io
+	PRIVATE
+    ${CPUINFO_LIBRARY}
+)
+endif ()
+
+
 target_link_libraries (dbms
         PRIVATE
     clickhouse_parsers
diff --git a/dbms/src/Common/config.h.in b/dbms/src/Common/config.h.in
index 302fc33..157a2d6 100644
--- a/dbms/src/Common/config.h.in
+++ b/dbms/src/Common/config.h.in
@@ -17,3 +17,4 @@
 #cmakedefine01 CLICKHOUSE_SPLIT_BINARY
 #cmakedefine01 USE_BASE64
 #cmakedefine01 USE_HDFS
+#cmakedefine01 USE_CPUINFO
diff --git a/dbms/src/Common/getNumberOfPhysicalCPUCores.cpp b/dbms/src/Common/getNumberOfPhysicalCPUCores.cpp
index 0a686b9..9832574 100644
--- a/dbms/src/Common/getNumberOfPhysicalCPUCores.cpp
+++ b/dbms/src/Common/getNumberOfPhysicalCPUCores.cpp
@@ -1,45 +1,25 @@
 #include <Common/getNumberOfPhysicalCPUCores.h>
 #include <thread>
 
-#if defined(__x86_64__)
-
-    #include <libcpuid/libcpuid.h>
-    #include <Common/Exception.h>
-
-    namespace DB { namespace ErrorCodes { extern const int CPUID_ERROR; }}
-
-#endif
+#include <Common/config.h>
 
+#if USE_CPUINFO
+#include <cpuinfo.h>
 
 unsigned getNumberOfPhysicalCPUCores()
 {
-#if defined(__x86_64__)
-    cpu_raw_data_t raw_data;
-    if (0 != cpuid_get_raw_data(&raw_data))
-        throw DB::Exception("Cannot cpuid_get_raw_data: " + std::string(cpuid_error()), DB::ErrorCodes::CPUID_ERROR);
-
-    cpu_id_t data;
-    if (0 != cpu_identify(&raw_data, &data))
-        throw DB::Exception("Cannot cpu_identify: " + std::string(cpuid_error()), DB::ErrorCodes::CPUID_ERROR);
-
-    /// On Xen VMs, libcpuid returns wrong info (zero number of cores). Fallback to alternative method.
-    if (data.num_logical_cpus == 0)
-        return std::thread::hardware_concurrency();
-
-    unsigned res = data.num_cores * data.total_logical_cpus / data.num_logical_cpus;
-
-    /// Also, libcpuid gives strange result on Google Compute Engine VMs.
-    /// Example:
-    ///  num_cores = 12,            /// number of physical cores on current CPU socket
-    ///  total_logical_cpus = 1,    /// total number of logical cores on all sockets
-    ///  num_logical_cpus = 24.     /// number of logical cores on current CPU socket
-    /// It means two-way hyper-threading (24 / 12), but contradictory, 'total_logical_cpus' == 1.
-
-    if (res != 0)
-        return res;
-#endif
-
-    /// As a fallback (also for non-x86 architectures) assume there are no hyper-threading on the system.
-    /// (Actually, only Aarch64 is supported).
-    return std::thread::hardware_concurrency();
+        uint32_t cores = 0;
+        if (cpuinfo_initialize())
+                cores = cpuinfo_get_cores_count();
+
+        if (cores)
+                return cores;
+        else
+                return std::thread::hardware_concurrency();
 }
+#else
+unsigned getNumberOfPhysicalCPUCores()
+{
+	return std::thread::hardware_concurrency();
+}
+#endif
